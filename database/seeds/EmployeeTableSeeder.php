<?php

use Illuminate\Database\Seeder;
use App\Employee;
use App\Education;
use App\User;
use App\Role;

class EmployeeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // มานะ
        $employee['code']         = "0001";
        $employee['prename']      = 1;
        $employee['name']         = "มานะ";
        $employee['surname']      = "สดใส";
        $employee['birthday']     = "1992-09-12";
        $employee['age']          = "27";
        $employee['blood']        = 2;
        $employee['race']         = "1";
        $employee['nationality']  = "1";
        $employee['religion']     = "1";
        $employee['address']      = "82/333454";
        $employee['card_id']      = "1254432556523";
        $employee['tel']          = "0945325456";
        $employee['type']         = "1";
        $employee['position']     = "การเงิน";
        $employee['salary']       = "25000";
        $employee['clock_status'] = 0;

        $last = Employee::create($employee);

        $education['employee_id'] = $last->id;
        $education['graduate']    = "CMRU";
        $education['study']       = "IT";
        $education['education']   = "3";
        $education['grade']       = "3.17";

        Education::create($education);

        $user = new User();
        $user->name     = $last->name." ".$last->surname;
        $user->email    = "mana@gmail.com";
        $user->username = $last->code;
        $user->password = bcrypt($last->code);
        $user->image    = "user.png";
        $user->save();
        $user->roles()->attach(Role::where('name', 'User')->first());


          // มานี
          $employee['code']         = "0002";
          $employee['prename']      = 2;
          $employee['name']         = "มานี";
          $employee['surname']      = "ดีใจ";
          $employee['birthday']     = "1992-09-12";
          $employee['age']          = "27";
          $employee['blood']        = 1;
          $employee['race']         = "1";
          $employee['nationality']  = "1";
          $employee['religion']     = "1";
          $employee['address']      = "82/333454";
          $employee['card_id']      = "1254432556511";
          $employee['tel']          = "0945325477";
          $employee['type']         = "2";
          $employee['position']     = "คลังสินค้า";
          $employee['salary']       = "20000";
          $employee['clock_status'] = 0;
  
          $last = Employee::create($employee);
  
          $education['employee_id'] = $last->id;
          $education['graduate']    = "CMRU";
          $education['study']       = "การตลาด";
          $education['education']   = "3";
          $education['grade']       = "3.17";
  
          Education::create($education);
  
          $user = new User();
          $user->name     = $last->name." ".$last->surname;
          $user->email    = "manee@gmail.com";
          $user->username = $last->code;
          $user->password = bcrypt($last->code);
          $user->image    = "user.png";
          $user->save();
          $user->roles()->attach(Role::where('name', 'User')->first());
          
    }
}
