<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_user = new Role();
        $role_user->name = 'User';
        $role_user->description = 'พนักงาน';
        $role_user->save();

        $role_admin = new Role();
        $role_admin->name = 'Admin';
        $role_admin->description = 'ผู้ดูแลระบบ';
        $role_admin->save();

        $role_admin = new Role();
        $role_admin->name = 'Manager';
        $role_admin->description = 'ผู้บริหาร';
        $role_admin->save();
    }
}
