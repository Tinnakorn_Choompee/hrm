<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(RoleTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(TimeTableSeeder::class);
        $this->call(PositionTableSeeder::class);
        $this->call(HolidayTableSeeder::class);
        $this->call(LeaveTypeTableSeeder::class);
        $this->call(EmployeeTableSeeder::class);
    }
}

