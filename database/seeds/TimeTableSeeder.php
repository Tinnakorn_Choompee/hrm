<?php

use Illuminate\Database\Seeder;
use App\Time;

class TimeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $time = new Time();
        $time->type = 'start';
        $time->time = '08:00';
        $time->save();

        $time = new Time();
        $time->type = 'end';
        $time->time = '10:00';
        $time->save();

        $time = new Time();
        $time->type = 'late';
        $time->time = '08:30';
        $time->save();

        $time = new Time();
        $time->type = 'out';
        $time->time = '17:00';
        $time->save();
    }
}
