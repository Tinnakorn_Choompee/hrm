<?php

use Illuminate\Database\Seeder;
use App\Holiday;

class HolidayTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            "2019-01-01" => "วันขึ้นปีใหม่",
            "2019-02-19" => "วันมาฆบูชา",
            "2019-04-07" => "วันจักรี",
            "2019-04-08" => "วันหยุดชดเชยวันจักรี",
            "2019-05-01" => "วันแรงงาน",
        ];

        foreach ($data as $date => $name) {
            Holiday::create(['date'=> $date, 'name' => $name]);
        }
    }
}
