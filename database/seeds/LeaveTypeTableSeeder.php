<?php

use Illuminate\Database\Seeder;
use App\LeaveType;

class LeaveTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            1 => 'การลาป่วย',
            2 => 'การลาคลอดบุตร',
            3 => 'การลากิจส่วนตัว',
            4 => 'การลาพักผ่อน',
            5 => 'การลาอุปสมบท',
            6 => 'การลาไปศึกษา',
        ];

        foreach ($data as $key => $value) {
            LeaveType::create(['type_no' => $key , 'type_name' => $value]);
        }
    }
}
