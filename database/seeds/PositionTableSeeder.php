<?php

use Illuminate\Database\Seeder;
use App\Position;

class PositionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $position = new Position();
        $position->position_no   = 1;
        $position->position_name = 'ผู้จัดการ';
        $position->save();

        $position = new Position();
        $position->position_no   = 2;
        $position->position_name = 'บัญชี';
        $position->save();

        $position = new Position();
        $position->position_no   = 3;
        $position->position_name = 'การเงิน';
        $position->save();

        $position = new Position();
        $position->position_no   = 4;
        $position->position_name = 'คลังสินค้า';
        $position->save();
    }
}
