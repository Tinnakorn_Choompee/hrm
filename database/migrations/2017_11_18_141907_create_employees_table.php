<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code',10);
            $table->tinyInteger('prename');
            $table->string('name', 100);
            $table->date('birthday');
            $table->integer('age');
            $table->tinyInteger('blood');
            $table->tinyInteger('race');
            $table->tinyInteger('nationality');
            $table->tinyInteger('religion');
            $table->text('address');
            $table->string('card_id',13);
            $table->string('tel', 10);
            $table->tinyInteger('type');
            $table->string('position', 100);
            $table->integer('salary');
            $table->string('image', 191)->default("user.png");
            $table->timestamps();
        });
        // DB::statement('ALTER TABLE employees CHANGE code code INT(4) UNSIGNED ZEROFILL NOT NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
