<!-- Modal Show-->
<div id="show" class="modal fade" role="dialog">
	<div class="modal-dialog" style="margin-top:5%">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<i class="fa fa-text-width"></i>
				<span class="modal-title font" style="font-size:25px;margin-left:10px"></span>
			</div>
            <hr>
			<div class="modal-body">
				<dl class="font" style="font-size:20px">
					<dt>พนักงาน</dt>
					<dd id="employee"></dd>
					<br>
					<dt>ประเภท</dt>
					<dd id="type"></dd>
					<br>
					<dt>รายละเอียด</dt>
					<dd class="message" id="detail"></dd>
					<br>
					<dt>วันที่</dt>
					<dd id="date"></dd>
				</dl>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
    </div>
</div>
