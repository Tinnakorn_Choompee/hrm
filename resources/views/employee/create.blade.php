@extends('layouts.app')
@section('title', 'เพิ่มพนักงาน')
@section('styles')
    <!-- flatpickr -->
    {{ Html::style('plugins/flatpickr/flatpickr.min.css') }}
@endsection
@section('content')
    <!-- Content Wrapper Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        @include('layouts.Backend.breadcrumb', ['title'=>'เพิ่มพนักงาน'])
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12 col-xs-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                        <h3 class="box-title font">ข้อมูลพนักงาน</h3>
                        </div>
                        {{--  @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif  --}}
                        <!-- /.box-header -->
                        <!-- form start -->
                        {!! Form::open(['url' => 'employee', 'files'=>TRUE]) !!}
                            <div class="box-body">

                                <div class="row">
                                    <div class="col-md-2 col-xs-12">
                                        <div class="form-group">
                                            {!! Form::label('code', 'เลขที่พนักงาน', ['class'=>'label_font']) !!}
                                            {!! Form::text('code', $code, ['class'=>'form-control','readonly']) !!}
                                            <div class="togglebutton">
                                                <label>
                                                   {!! Form::checkbox(NULL, NULL, FALSE,['id'=>'is_fix_code', 'data-code'=> $code]) !!}
                                                   <span class="font" style="font-size:20px"> กำหนดเอง </span>
                                                </label>
                                            </div>
                                        </div>
                                        @if ($errors->has('code'))
                                        <span class="help-block text-danger">
                                            <strong> {{ $errors->first('code') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="col-md-1 col-xs-12">
                                        <div class="form-group">
                                            {!! Form::label('prename', 'คำนำหน้า', ['class'=>'label_font']) !!}
                                            {!! Form::select('prename', $data['prename'] , NULL, ['class'=>'form-control', 'required', 'autofocus']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-xs-12">
                                        <div class="form-group">
                                            {!! Form::label('name', 'ชื่อพนักงาน', ['class'=>'label_font']) !!}
                                            {!! Form::text('name',  old('name') ,['class'=>'form-control', 'required']) !!}
                                        </div>
                                        @if ($errors->has('name'))
                                        <span class="help-block text-danger">
                                            <strong> {{ $errors->first('name') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="col-md-3 col-xs-12">
                                        <div class="form-group">
                                            {!! Form::label('surname', 'นามสกุล', ['class'=>'label_font']) !!}
                                            {!! Form::text('surname',  old('surname') ,['class'=>'form-control', 'required']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-xs-6">
                                        <div class="form-group">
                                            {!! Form::label('birthday', 'วันเกิด', ['class'=>'label_font']) !!}
                                            {!! Form::date('birthday',  NULL, ['class'=>'form-control', 'required']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-1 col-xs-6">
                                        <div class="form-group">
                                            {!! Form::label('age', 'อายุ', ['class'=>'label_font']) !!}
                                            {!! Form::text('age',  0, ['class'=>'form-control','readonly']) !!}
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3 col-xs-6">
                                        <div class="form-group">
                                            {!! Form::label('blood', 'กรุ๊ปเลือด', ['class'=>'label_font']) !!}
                                            {!! Form::select('blood', $data['blood'] , NULL, ['class'=>'form-control', 'required']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-xs-6">
                                        <div class="form-group">
                                            {!! Form::label('race', 'เชื่อชาติ', ['class'=>'label_font']) !!}
                                            {!! Form::select('race', $data['race'] , NULL, ['class'=>'form-control', 'required']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-xs-6">
                                        <div class="form-group">
                                            {!! Form::label('nationality', 'สัญชาติ', ['class'=>'label_font']) !!}
                                            {!! Form::select('nationality', $data['nationality'] , NULL, ['class'=>'form-control', 'required']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-xs-6">
                                        <div class="form-group">
                                            {!! Form::label('religion', 'ศาสนา', ['class'=>'label_font']) !!}
                                            {!! Form::select('religion', $data['religion'] , NULL, ['class'=>'form-control', 'required']) !!}
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3 col-xs-12">
                                        <div class="form-group">
                                            {!! Form::label('address', 'ที่อยู่', ['class'=>'label_font']) !!}
                                            {!! Form::textarea('address', old('address'), ['class'=>'form-control', 'rows'=> 1, 'required']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-xs-12">
                                        <div class="form-group">
                                            {!! Form::label('email', 'อีเมล์', ['class'=>'label_font']) !!}
                                            {!! Form::email('email', old('email'), ['class'=>'form-control', 'required']) !!}
                                        </div>
                                        @if ($errors->has('email'))
                                        <span class="help-block text-danger">
                                            <strong> {{ $errors->first('email') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="col-md-3 col-xs-6">
                                        <div class="form-group">
                                            {!! Form::label('card_id', 'เลขบัตรประจำตัว', ['class'=>'label_font']) !!}
                                            {!! Form::tel('card_id', old('card_id'), ['class'=>'form-control', 'maxlength'=> 13 ,'required']) !!}
                                        </div>
                                        @if ($errors->has('card_id'))
                                        <span class="help-block text-danger">
                                            <strong> {{ $errors->first('card_id') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="col-md-3 col-xs-6">
                                        <div class="form-group">
                                            {!! Form::label('tel', 'เบอร์โทร', ['class'=>'label_font']) !!}
                                            {!! Form::tel('tel', old('tel'), ['class'=>'form-control', 'maxlength'=> 10, 'required']) !!}
                                            <p id="asdasd"></p>
                                        </div>
                                        @if ($errors->has('tel'))
                                        <span class="help-block text-danger">
                                            <strong> {{ $errors->first('tel') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row">
                                     <div class="col-md-3 col-xs-6">
                                        <div class="form-group">
                                            {!! Form::label('type', 'ประเภทพนักงาน', ['class'=>'label_font']) !!}
                                            {!! Form::select('type', $data['type'] , NULL, ['class'=>'form-control', 'required']) !!}
                                            <span ></span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-xs-6">
                                        <div class="form-group">
                                            {!! Form::label('position', 'ตำแหน่งพนักงาน', ['class'=>'label_font']) !!}
                                            {!! Form::select('position', $position , NULL, ['class'=>'form-control', 'required']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-xs-6">
                                        <div class="form-group">
                                            {!! Form::label('salary', 'เงินเดือน (ต่อ week)', ['class'=>'label_font','id'=>'text']) !!}
                                            {!! Form::number('salary', old('salary'), ['class'=>'form-control','required', 'placeholder'=>'บาท']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-xs-6">
                                        <div class="form-group is-fileinput">
                                            {!! Form::label('image', 'รูปพนักงาน', ['class'=>'label_font']) !!}
                                            {!! Form::text( NULL,  NULL, ['class'=>'form-control','readonly', 'placeholder'=>'เลือกรูปภาพ']) !!}
                                            {!! Form::file('image', ['class'=>'form-control']); !!}
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3 col-xs-12">
                                        <div class="form-group">
                                            {!! Form::label('graduate', 'สถานที่จบการศึกษา', ['class'=>'label_font']) !!}
                                            {!! Form::text('graduate', old('graduate'), ['class'=>'form-control', 'required']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-xs-6">
                                        <div class="form-group">
                                            {!! Form::label('study', 'สาขา', ['class'=>'label_font']) !!}
                                            {!! Form::text('study', old('study') , ['class'=>'form-control']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-xs-6">
                                        <div class="form-group">
                                            {!! Form::label('education', 'วุฒิการศึกษา', ['class'=>'label_font']) !!}
                                            {!! Form::select('education', $data['education'] , NULL, ['class'=>'form-control', 'required']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-xs-6">
                                        <div class="form-group">
                                            {!! Form::label('grade', 'เกรดเฉลี่ย', ['class'=>'label_font']) !!}
                                            {!! Form::number('grade', old('grade'), ['class'=>'form-control', 'step'=>0.01 ,'max'=> 4  ,'placeholder'=>'0.00']) !!}
                                        </div>
                                    </div>
                                </div>

                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary label_font"> บันทึก </button>
                            </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- /.box -->
            </div><!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection
@push('scripts')
   <!-- flatpickr -->
   {{ Html::script('plugins/flatpickr/flatpickr.min.js') }}
   {{ Html::script('plugins/flatpickr/rtl/th.js') }}
   <script>
    $('#card_id').keypress(function () {
        var maxLength = $(this).val().length;
        if (maxLength >= 13) { return false;}
    });
    $('#tel').keypress(function () {
        var maxLength = $(this).val().length;
        if (maxLength >= 10) { return false;}
    });

    $( "#type" ).change(function () {
        var str = " ";
        $( "#type option:selected" ).each(function() {
        if ($(this).val() == 1) {
          str += "เงินเดือน (ต่อ week)";
        } else {
          str += "รายวัน (ต่อวัน)";
        }
      });
      $( "#text" ).text(str);
    });
    {{--  .change()  --}}

   	$("#is_fix_code").click('checked', function() {
		var code = $(this).data('code');
		if($(this).prop('checked')) {
			$("#code").val("");
			$("#code").prop("readonly", false);
			$("#code").focus();
		}
		else {
			$("#code").val(code);
			$("#code").prop("readonly", true);
		}
	});
    var date  = new Date;
    var day   = date.getDate();
    var month = date.getMonth() + 1;
    var year  = date.getUTCFullYear();
    function getAge(d1, d2){
		d2 = d2 || new Date();
		var diff = d2.getTime() - d1.getTime();
		var age = Math.floor(diff / (1000 * 60 * 60 * 24 * 365.25));
		return isNaN(diff) ? "" : (age < 0 ? 0 : age);
	}
    const birthday = flatpickr("#birthday", { defaultDate: date, locale : 'th', maxDate: new Date() });
    $("#birthday").change(function() {
		var arr_date = $(this).val().split("-");
		$("#age").val(getAge(new Date((arr_date[0]), arr_date[1], arr_date[2])));
	});
   </script>
@endpush
