<!-- Modal -->
<div id="create" class="modal fade" role="dialog">
  <div class="modal-dialog" style="margin-top:10%">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title font" style="font-size:22px"> <span class="text-red"> ขณะนี้คุณได้ออกงานก่อนเวลาเลิกงาน </span> โปรดระบุข้อมูลการลา </h4>
      </div>
      <div class="modal-body">
        {!! Form::open(['url' => '/leave', 'class'=> 'form-horizontal']) !!}
        {!! Form::hidden('employee_id', NULL, ['class'=>'id']) !!}
        {!! Form::hidden('page', NULL, ['class'=>'page']) !!}
          <div class="row">
            <div class="form-group">
              {!! Form::label('type', 'ประเภทการลา', ['class'=>'label_font col-sm-3 text-right']) !!}
              <div class="col-sm-7">
                <select name="type_name" id="leavetype" class="form-control">
                    <option value="" selected disabled>เลือกประเภท</option>
                    @foreach($leavetype as $k => $v)
                      <option value="{{$k}}" id="{{$k}}">{{$v}}</option>
                    @endforeach
                </select>
              </div>
            </div>
            
            <div class="form-group">
              {!! Form::label('type', 'ชนิดการลา', ['class'=>'label_font col-sm-3 text-right']) !!}
              <div class="col-sm-7">
                <div class="radio">
                   <label style="margin-right:5px"> 
                      {!! Form::radio('type_leave', 3, true, ['class'=> 'radio-full']) !!} เต็มวัน
                 </label>
              </div>
              </div>
            </div>

            <div class="half" style="display:none">
              <div class="form-group">
                {!! Form::label('type', 'วันที่', ['class'=>'label_font col-sm-3 text-right']) !!}
                <div class="col-sm-7">
                  {!! Form::date('start', NULL , ['class'=>'form-control c_clock', 'required']) !!}
                  {!! Form::hidden('end', date('Y-m-d')) !!}
                </div>
              </div> 
            </div> 

            <div class="full">
              <div class="form-group">
                {!! Form::label('type', 'ช่วงเวลา', ['class'=>'label_font col-sm-3 text-right']) !!}
                <div class="col-sm-3">
                  {!! Form::date('start', NULL , ['class'=>'form-control c_clock', 'required']) !!}
                
                </div>
                {!! Form::label('type', 'ถึง', ['class'=>'label_font col-sm-1 text-center']) !!}
                <div class="col-sm-3">
                  {!! Form::date('end', NULL , ['class'=>'form-control c_clock',  'required']) !!}
   
                </div>
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('detail', 'รายละเอียด', ['class'=>'label_font col-sm-3 text-right']) !!}
              <div class="col-sm-7">
                {!! Form::textarea('detail', NULL, ['class'=>'form-control', 'rows'=> 3, 'required']) !!}
              </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
