
<section class="section about bg-gray" id="About">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-6">
				<div class="media response-about">
					<div class="media-body" style="padding-right:20px;">
						<div class="font-about"> ร้านรุ่งเรืองทรัพย์ อะไหล่เครื่องหนัง</div>
						<p>
							รับออกแบบและผลิตอะไหล่เครื่องหนังตามออเดอร์ ตัวแทนจำหน่ายอะไหล่เครื่องหนัง อะไหล่กระเป๋า อะไหล่รองเท้า อะไหล่เข็มขัด อุปกรณ์เครื่องหนัง
							อุปกรณ์กระเป๋า อุปกรณ์รองเท้า อุปกรณ์เข็มขัด หัวเข็มขัด กระดุมโลหะ หางซิป จี้หัวห้อยกระเป๋า จี้หัวห้อยรองเท้า โซ่โลหะ
							โซ่หูกระเป๋า อุปกรณ์ปรับสาย
						</p>
						<div class="font-about"> ที่อยู่ </div>
						<p>
							193 ถนนเจริญรัถ แขวงคลองสาน เขตคลองสาน กทม 10600
						</p>
						<div class="font-about">
							โทรศัพท์
						</div>
						<p>
							0-2438-7623, 08-9515-6442
						</p>
						<p>
							Email : roongruangsarp@hotmail.com
						</p>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-6">
				<div id="carouselExampleIndicators" class="carousel slide mr-5" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
							<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
							<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
						</ol>
						<div class="carousel-inner">
							<div class="carousel-item active">
								<img class="d-block w-100" src="images/about/1.jpg" alt="">
							</div>
							<div class="carousel-item">
								<img class="d-block w-100" src="images/about/2.jpg" alt="">
							</div>
							<div class="carousel-item">
								<img class="d-block w-100" src="images/about/3.jpg" alt="">
							</div>
						</div>

						<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						</a>
					</div>
			</div>
		</div>
	</div>
</section>
