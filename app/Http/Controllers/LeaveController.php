<?php

namespace App\Http\Controllers;

use App\Leave;
use App\Clocktime;
use App\Employee;
use App\Work;
use App\Holiday;
use App\LeaveType;
use Illuminate\Http\Request;
use Libraries\EmployeeLibrary\EmployeeLibrary;
use Libraries\LeaveLibrary\LeaveLibrary;
use Libraries\DateThaiLibrary\DateThaiLibrary;
use Carbon;

class LeaveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data      = EmployeeLibrary::Data();
        $leave     = Leave::with('employee')->get();
        $status    = LeaveLibrary::Status();
        $holiday   = Holiday::all();
        $leavetype = LeaveType::orderBy('type_no')->pluck('type_name','type_name');
        return view('leave.index')->withLeave($leave)->withData($data)->withStatus($status)->withHoliday($holiday)->withLeavetype($leavetype);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // สถานะการลา  0 ไม่อนุมัติ  1 รอการพิจารณา  2 ทำการอนุมัติเรียบร้อย
        switch ($request->page) {
            case "clocktime" :
                $diff_day = Carbon::parse($request->start)->diffInDays(Carbon::parse($request->end));    
                switch ($request->type_leave) :
                case 1 : $num = 0.5;           break;
                case 2 : $num = 0.5;           break;
                case 3 : $num = $diff_day + 1; break;
                endswitch;
                $leave = $request->except(['page']);
                $leave['num'] = $num;
                Leave::create($leave);
                $this->clock_out($request->employee_id);
                    
                $this->leave($request->employee_id);

                return redirect('/clocktime')->with('leave', 'Leave Successfully!');
            break;

            case "leave" :
            
                $employee = Employee::where('code', $request->employee_id)->first();
                $diff_day = Carbon::parse($request->start)->diffInDays(Carbon::parse($request->end));    

                switch ($request->type_leave) :
                case 1 : $num = 0.5;           break;
                case 2 : $num = 0.5;           break;
                case 3 : $num = $diff_day + 1; break;
                endswitch;

                $leave = new Leave;
                $leave->employee_id = $employee->id;
                $leave->type_name   = $request->type_name;
                $leave->type_leave  = $request->type_leave;
                $leave->num         = $num;
                $leave->start       = $request->start;
                $leave->end         = $request->end;
                $leave->detail      = $request->detail;
                $leave->save();

                $work = new Work;
                $work->employee_id = $employee->id;
                $work->status      = 3;
                $work->date        = $request->start;
                $work->save();

                return redirect("/employee/profile/{$employee->code}")->with('leave', 'Leave Successfully!');
            break;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Leave  $leave
     * @return \Illuminate\Http\Response
     */
    public function show(Leave $leave)
    {
        $data   = EmployeeLibrary::Data();
        $status = LeaveLibrary::Status();
        $leave  = Leave::with('employee')->find($leave->id);
        switch ($leave->type_leave) :
            case 1 : $leave->type_leave = "ครึ่งเช้า"; break;
            case 2 : $leave->type_leave = "ครึ่งเช้า"; break;
            case 3 : $leave->type_leave = "เต็มวัน";  break;
        endswitch;
        $leave->start  = DateThaiLibrary::ThaiDate($leave->start);
        $leave->end    = DateThaiLibrary::ThaiDate($leave->end);
        $leave->status = $status[$leave->status]['status'];
        $leave->employee->prename = $data['prename'][$leave->employee->prename];
        return response($leave);
    }

    public function profile($code)
    {
        $employee  = Employee::where('code', $code)->first();
        $data      = EmployeeLibrary::Data();
        $leave     = Leave::with('employee')->where('employee_id', $employee->id)->get();
        $status    = LeaveLibrary::Status();
        $holiday   = Holiday::all();
        $leavetype = LeaveType::orderBy('type_no')->pluck('type_name','type_name');
        return view('leave.profile')->withLeave($leave)->withData($data)->withStatus($status)->withHoliday($holiday)->withLeavetype($leavetype);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Leave  $leave
     * @return \Illuminate\Http\Response
     */
    public function edit(Leave $leave)
    {
        $data    = EmployeeLibrary::Data();
        $leave   = Leave::with('employee')->find($leave->id);
        $leave->employee->prename = $data['prename'][$leave->employee->prename];
        return response($leave);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Leave  $leave
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $page)
    {
        $leave = $request->all();
  
        $diff_day = Carbon::parse($request->start)->diffInDays(Carbon::parse($request->end));    

        switch ($request->type_leave) :
        case 1 : $num = 0.5;           break;
        case 2 : $num = 0.5;           break;
        case 3 : $num = $diff_day + 1; break;
        endswitch;

        $leave['num'] = $num;
    
        switch ($page) {
            case "leave" :
                Leave::updateOrCreate(['id' => $request->id] , $leave);
                return redirect('/leave')->with('update', '๊Update Successfully!');
            break;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Leave  $leave
     * @return \Illuminate\Http\Response
     */
    public function destroy(Leave $leave)
    {
        $date = date('Y-m-d', strtotime($leave->start));
        // $work = Work::where('employee_id', $leave->employee_id)->whereDate('date', $date)->first();
        // $work->status = 0;
        // $work->save();
        Work::where('employee_id', $leave->employee_id)->whereDate('date', $date)->delete();
        Leave::destroy($leave->id);
    }

    public function clock_out($id)
    {
        $clock_out = Clocktime::where('employee_id', $id)->whereDate('clock_in', now()->toDateString())->first();
        $clock_out->clock_out = now();
        $clock_out->clock_status = 2;
        $clock_out->save();

        $employee = Employee::find($id);
        $employee->clock_status = 2;
        $employee->save();
    }

    public function leave($id)
    {
        $work = Work::where('employee_id', $id)->whereDate('date', date('Y-m-d'))->first();
        $work->status = 3;
        $work->save();
        // status   3 = ลา
    }

    public function status(Request $request)
    {
        $leave = Leave::find($request->id);
        switch ($request->status) {
            case 0 :
                $date = date('Y-m-d', strtotime($leave->start));
                $work = Work::where('employee_id', $leave->employee_id)->whereDate('date',  $date)->first();
                $work->status = 0;
                $work->save();
                $status = "not";
            break;

            case 2 :
                $date = date('Y-m-d', strtotime($leave->start));
                $work = Work::where('employee_id', $leave->employee_id)->whereDate('date',  $date)->first();
                $work->status = 3;
                $work->save();
                $status = "approve";
            break;
        }

        $leave = Leave::find($request->id);
        $leave->status = $request->status;
        $leave->save();

        return redirect('/leave')->with($status, 'Save Successfully!');
    }
}
