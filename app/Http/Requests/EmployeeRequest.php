<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;


class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'code'        => 'required|unique:employees,code,'.$request->id,
            'prename'     => 'required|numeric',
            'name'        => 'required|string',
            'surname'     => 'required|string', 
            'birthday'    => 'required',
            'age'         => 'required|numeric',
            'blood'       => 'required|numeric',
            'race'        => 'required|numeric',
            'nationality' => 'required|numeric',
            'religion'    => 'required|numeric',
            'email'       => 'required|unique:users,email,'.$request->user_id,
            'card_id'     => 'required|numeric|unique:employees,card_id,'.$request->id,
            'tel'         => 'required|numeric',
            'type'        => 'required|numeric',
            'position'    => 'required|string',
            'salary'      => 'required|numeric',  
            'image'       => 'nullable|mimes:jpeg,png,jpg',
            'graduate'    => 'required|string', 
            'study'       => 'nullable|string', 
            'education'   => 'required|numeric',
            'grade'       => 'nullable'   
        ];
    }
    
    public function messages()
    {
        return [
            'code.unique' => 'เลขที่พนักงานซ้ำกัน',
            'card_id.unique' => 'เลขบัตรพนักงานซ้ำกัน',
            'email.unique' => 'มีผู้ใช้อีเมล์นี้แล้ว',
            'card_id.numeric' => 'กรุณาป้อนข้อมูลเป็นตัวเลข',
            'tel.numeric' => 'กรุณาป้อนข้อมูลเป็นตัวเลข',
        ];
    }
}